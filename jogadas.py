import turtle
import time
import sys
from jogoDosOitoOO import Tabuleiro,JogoDosOito
trtl=turtle.Turtle()
trtlnum=turtle.Turtle()
trtl.ht()
trtlnum.ht()

def desenha_tabu():
  trtl.speed(0)
  for i in range(0,400,133):
    trtl.pencolor('lightgrey')
    trtl.penup()
    trtl.setpos(-200+i,-200)
    if i==0:
      trtl.left(90)
    trtl.pendown()
    trtl.forward(400)
    trtl.backward(400)
  for i in range(0,400,133):
    trtl.pencolor('lightgrey')
    trtl.penup()
    trtl.setpos(-200,-200+i)
    if i==0:
      trtl.right(90)
    trtl.pendown()
    trtl.forward(400)
    trtl.backward(400) 
  trtl.penup()


def desenha_num():
  x =0
  y = 0
  trtlnum.clear()
  trtlnum.speed(0)
  for i in range(0,400,134):
    for j in range(0,400,134):
      trtlnum.penup()
      trtlnum.setpos(-200+50+j,200-50-i)
      trtlnum.pendown()
      var = m[y%3][x%3]
      if m[y%3][x%3] == 0:
        var = ''

      trtlnum.write(var, font=("Arial", "20", "bold"))
      x = x+1
    y +=1
    
# m = [["1","2","3"],["4","5","6"],["7","8"," " ]]
# desenha_tabu()
# desenha_num()
# time.sleep(2)
# m = [["1","2","3"],["4","5","6"],["7","","8" ]]
# desenha_num()
# time.sleep(2)
arquivo = open('tabuleiros\\' + sys.argv[1], 'r')
m = []
LARGURA = 1
PROFUNDIDADE = 2
GULOSO = 3
A_ESTRELA = 4
for linha in arquivo.readlines():
  linha_temp = linha.replace("\n", "").split(",")
  nova_linha = []
  for item in linha_temp:
    if item != '':
      item = int(item)
    else:
      item = 0
    nova_linha.append(item)
  m.append(nova_linha)

desenha_tabu()
tabuleiro = Tabuleiro(m, 0, None)
jogo_dos_oito = JogoDosOito()
if int(sys.argv[2]) == LARGURA:
  busca = jogo_dos_oito.busca_cega_busca_largura(tabuleiro)
elif int(sys.argv[2]) == PROFUNDIDADE:
  busca = jogo_dos_oito.busca_cega_busca_profundidade(tabuleiro)
elif int(sys.argv[2]) == GULOSO:
  busca = jogo_dos_oito.busca_heuristica(tabuleiro)
elif int(sys.argv[2]) == A_ESTRELA:
  busca = jogo_dos_oito.a_estrela(tabuleiro)
print(busca)  
lista = jogo_dos_oito.historico_de_jogadas(busca)

for tabuleiro in lista[::-1]:
  m = tabuleiro.tab
  desenha_num()
  time.sleep(2)