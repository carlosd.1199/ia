import copy
import time
import sys
import random

def valor(item):
    return item.valor_da_funcao_F

class Tabuleiro:
    def __init__(self, tab, nivel, tabuleiro_pai):
        self.tab = tab
        self.nivel = nivel
        self.tabuleiro_pai = tabuleiro_pai
        self.valor_da_funcao_F = -1


class JogoDosOito:
    def __init__(self):
        self.nome = "Jogo Dos 8"

    def verificar_se_acabou(self, Tabuleiro):
        if Tabuleiro.tab[0][0] != 1:
            return False
        if Tabuleiro.tab[0][1] != 2:
            return False
        if Tabuleiro.tab[0][2] != 3:
            return False
        if Tabuleiro.tab[1][0] != 4:
            return False
        if Tabuleiro.tab[1][1] != 5:
            return False
        if Tabuleiro.tab[1][2] != 6:
            return False
        if Tabuleiro.tab[2][0] != 7:
            return False
        if Tabuleiro.tab[2][1] != 8:
            return False
        return True
    
    def mostrar_tabuleiro(self, Tabuleiro): 
        print('Nível do nó:')
        print(Tabuleiro.nivel)
        print(Tabuleiro.tab[0])
        print(Tabuleiro.tab[1])
        print(Tabuleiro.tab[2])

    def esta_dentro_do_intervalo(self, i, j):
        if i > 2 or i < 0:
            return False
        if j > 2 or j < 0:
            return False
        return True

    def explorar_tabuleiro(self, Tabuleiro):
        novos_nos_na_fronteira = []

        for i in range(0, len(Tabuleiro.tab)):
            for j in range(0, len(Tabuleiro.tab)):
                if(Tabuleiro.tab[i][j] == 0):
                    linha = i
                    coluna = j

        #mover a peça de cima para a posição vazia
        if self.esta_dentro_do_intervalo(linha - 1, coluna):

            aux = copy.deepcopy(Tabuleiro)
            aux.tab[linha][coluna] = aux.tab[linha - 1][coluna]
            aux.tab[linha - 1][coluna] = 0
            aux.nivel = aux.nivel + 1
            aux.tabuleiro_pai = Tabuleiro
            novos_nos_na_fronteira.append(aux)

        #mover a peça da esquerda para a posição
        if self.esta_dentro_do_intervalo(linha, coluna - 1):
            aux = copy.deepcopy(Tabuleiro)
            aux.tab[linha][coluna] = aux.tab[linha][coluna - 1]
            aux.tab[linha][coluna - 1] = 0
            aux.nivel = aux.nivel + 1
            aux.tabuleiro_pai = Tabuleiro
            novos_nos_na_fronteira.append(aux)

        #mover a peça da direita para a posição vazia
        if self.esta_dentro_do_intervalo(linha, coluna + 1):
            aux = copy.deepcopy(Tabuleiro)
            aux.tab[linha][coluna] = aux.tab[linha][coluna + 1]
            aux.tab[linha][coluna + 1] = 0
            aux.nivel = aux.nivel + 1
            aux.tabuleiro_pai = Tabuleiro
            novos_nos_na_fronteira.append(aux)

        #mover a peça de baixo para a posição vazia
        if self.esta_dentro_do_intervalo(linha + 1, coluna):
            aux = copy.deepcopy(Tabuleiro)
            aux.tab[linha][coluna] = aux.tab[linha + 1][coluna]
            aux.tab[linha + 1][coluna] = 0
            aux.nivel = aux.nivel + 1
            aux.tabuleiro_pai = Tabuleiro
            novos_nos_na_fronteira.append(aux)
        
        return novos_nos_na_fronteira

    def historico_de_jogadas(self, Tabuleiro):
        hist = []
        hist.append(Tabuleiro)
        while Tabuleiro.tabuleiro_pai is not None:
            hist.append(Tabuleiro.tabuleiro_pai)
            Tabuleiro.tabuleiro_pai = Tabuleiro.tabuleiro_pai.tabuleiro_pai 
            #sim, essa parte é difícil de entender mesmo
            #acontece o seguinte, o Tabuleiro passado por parâmetro já está no histórico. No while vamos verificar se o Tabuleiro tem um pai, ou seja, um
            # Tabuleiro pai que o originou, se tiver, ele também irá para o histórico
        # for i in range(0, len(hist)):
        #     self.mostrar_tabuleiro(hist[i])
        return hist

    #Criei esse método para evitar percorrer o mesmo nó mais de uma vez, se ele forem iguais, esse nó não entra na minha fronteira de estados
    def verificar_se_eh_igual(self, Tabuleiro1, Tabuleiro2):
        for i in range(0, len(Tabuleiro1.tab)):
            for j in range(0, len(Tabuleiro1.tab)):
                if Tabuleiro1.tab[i][j] != Tabuleiro2.tab[i][j]:
                    return False
        return True
    
    def calcular_profundidade_maxima(self, repet):
        h = 0
        for i in repet:
            if i.nivel > h:
                h = i.nivel
        return h
    
    def sortear_filhos(self, Tabuleiro):
        novos_nos_na_fronteira = []

        for i in range(0, len(Tabuleiro.tab)):
            for j in range(0, len(Tabuleiro.tab)):
                if(Tabuleiro.tab[i][j] == 0):
                    linha = i
                    coluna = j
        
        numeros_sorteados = random.sample(range(0,4), 4)

        for i in numeros_sorteados:
            if i == 0:
                #mover a peça de cima para a posição vazia
                if self.esta_dentro_do_intervalo(linha - 1, coluna):
                    aux = copy.deepcopy(Tabuleiro)
                    aux.tab[linha][coluna] = aux.tab[linha - 1][coluna]
                    aux.tab[linha - 1][coluna] = 0
                    aux.nivel = aux.nivel + 1
                    aux.tabuleiro_pai = Tabuleiro
                    novos_nos_na_fronteira.append(aux)
            
            if i == 1:
                #mover a peça da esquerda para a posição
                if self.esta_dentro_do_intervalo(linha, coluna - 1):
                    aux = copy.deepcopy(Tabuleiro)
                    aux.tab[linha][coluna] = aux.tab[linha][coluna - 1]
                    aux.tab[linha][coluna - 1] = 0
                    aux.nivel = aux.nivel + 1
                    aux.tabuleiro_pai = Tabuleiro
                    novos_nos_na_fronteira.append(aux)
            
            if i == 2:
                #mover a peça da direita para a posição vazia
                if self.esta_dentro_do_intervalo(linha, coluna + 1):
                    aux = copy.deepcopy(Tabuleiro)
                    aux.tab[linha][coluna] = aux.tab[linha][coluna + 1]
                    aux.tab[linha][coluna + 1] = 0
                    aux.nivel = aux.nivel + 1
                    aux.tabuleiro_pai = Tabuleiro
                    novos_nos_na_fronteira.append(aux)
            
            if i == 3:
                #mover a peça de baixo para a posição vazia
                if self.esta_dentro_do_intervalo(linha + 1, coluna):
                    aux = copy.deepcopy(Tabuleiro)
                    aux.tab[linha][coluna] = aux.tab[linha + 1][coluna]
                    aux.tab[linha + 1][coluna] = 0
                    aux.nivel = aux.nivel + 1
                    aux.tabuleiro_pai = Tabuleiro
                    novos_nos_na_fronteira.append(aux)
        
        return novos_nos_na_fronteira


    def busca_cega_busca_profundidade(self, Tabuleiro):
        inicio = time.time()
        pilha = []
        pilha.append(Tabuleiro)
        # vou armazenar quantos nós foram expandidos
        quant_nos_visitados = 0
        profundidade = 0

        while len(pilha) > 0:
            
            tabAtual = pilha.pop(len(pilha) - 1)
            if tabAtual.nivel > profundidade:
                profundidade = tabAtual.nivel
            quant_nos_visitados = quant_nos_visitados + 1
            print('jogada atual')
            self.mostrar_tabuleiro(tabAtual)

            if self.verificar_se_acabou(tabAtual):
                fim = time.time()
                tempo = fim - inicio
                print('Número de nós testado: ')
                print(quant_nos_visitados)
                print('Tempo de execução: ')
                print(tempo)
                print('Custo da solução ou profundidade da solução: ')
                print(tabAtual.nivel)
                print('Profundidade máxima: ')
                print(profundidade)
                print('Fronteira de espaços de estados ou quantidade de nós gerados: ')
                print(quant_nos_visitados + len(pilha))
                return tabAtual
            else:
                filhos_de_tabAtual = self.sortear_filhos(tabAtual)
                # Nesse ponto, eu tenho todos os nós exploráveis a partir do tabuleiro Atual,
                # mas é interessante verificar se ele já foi explorado antes
                for i in filhos_de_tabAtual:
                    pilha.append(i)

    
    def busca_cega_busca_largura(self, Tabuleiro):
        inicio = time.time()
        fila = []
        filaDeRepetidos = []

        fila.append(Tabuleiro)
        filaDeRepetidos.append(Tabuleiro)

        #vou armazenar quantos nós foram expandidos
        quant_nos_visitados = 0

        while(len(fila) > 0 ):
            
            tabAtual = fila.pop(0)
            quant_nos_visitados = quant_nos_visitados + 1
            #print('jogada atual ---------------')

            self.mostrar_tabuleiro(tabAtual)
            if self.verificar_se_acabou(tabAtual):
                fim = time.time()
                tempo = fim - inicio
                #print("O jogo acabou !!!!!!")
                print('Número de nós testados: ')
                print(quant_nos_visitados)
                print('Tempo de execução: ')
                print(tempo)
                print('Custo da solução: ')
                print(tabAtual.nivel)
                print('Profundidade máxima: ')
                print(tabAtual.nivel)
                print('Fronteira de espaços de estado ou quantidade de nós gerados: ')
                print(quant_nos_visitados + len(fila))
                return tabAtual

            else:
                filhos_de_tabAtual = self.filhos_nao_repitidos(filaDeRepetidos, self.explorar_tabuleiro(tabAtual))
                # Nesse ponto, eu tenho todos os nós exploráveis a partir do tabuleiro Atual, 
                # mas é interessante verificar se ele já foi explorado antes
                for i in filhos_de_tabAtual:
                    fila.append(i)
                    filaDeRepetidos.append(i)
 
    #Heurística para a nossa busca gulosa
    #A heurística escolhida foi a 'Número de peças que estão fora do lugar'
    def heuristica_pecas_fora_do_lugar(self, Tabuleiro): 
        aux = 0
        if Tabuleiro.tab[0][0] != 1: 
            aux = aux + 1
        if Tabuleiro.tab[0][1] != 2: 
            aux = aux + 1
        if Tabuleiro.tab[0][2] != 3: 
            aux = aux + 1
        if Tabuleiro.tab[1][0] != 4: 
            aux = aux + 1
        if Tabuleiro.tab[1][1] != 5: 
            aux = aux + 1
        if Tabuleiro.tab[1][2] != 6: 
            aux = aux + 1
        if Tabuleiro.tab[2][0] != 7: 
            aux = aux + 1
        if Tabuleiro.tab[2][1] != 8: 
            aux = aux + 1
        return aux

    def filhos_nao_repitidos(self, repetidos, filhos):
        filhos_ineditos = []
        for i in filhos:
            novo = True
            for j in repetidos:
                if self.verificar_se_eh_igual(i, j):
                    novo = False
            if novo:
                filhos_ineditos.append(i)
        return filhos_ineditos




    def busca_heuristica(self, Tabuleiro):
        inicio = time.time()
        fronteira = []
        repetidos  = []
        fronteira.append(Tabuleiro)
        repetidos.append(Tabuleiro)
        nos_visitados = 0

        while len(fronteira) > 0:

            no_atual = fronteira.pop(0)
            print('no atual: ')
            print(self.mostrar_tabuleiro(no_atual))
            nos_visitados = nos_visitados + 1

            if self.verificar_se_acabou(no_atual):
                fim = time.time()
                tempo = fim - inicio
                print('Nós testados: ')
                print(nos_visitados)
                print('Tempo de execução: ')
                print(tempo)
                print('Custo da solução: ')
                print(no_atual.nivel)
                print('Profundidade máxima: ')
                print(no_atual.nivel)
                print('Custo dos nós na fronteira que é o número de nós espandidos: ')
                print(nos_visitados + len(fronteira))
                return no_atual
            else:

                filhos = self.filhos_nao_repitidos(repetidos, self.explorar_tabuleiro(no_atual))
                h = 9  # valor da heuristica escolhida não irá ultrapassar o limite de 9 
                # , mas é claro, quanto menor o valor dessa heurística melhor para o algortimo.
                # Dos nós filhos, só serão adicionados na fronteira o no/Tabuleiro que possui 
                # o menor 'H'

                for i in filhos:
                    aux = self.heuristica_pecas_fora_do_lugar(i)
                    if aux < h:
                        h = aux
                        
                for i in filhos:
                    if self.heuristica_pecas_fora_do_lugar(i) == h:
                        fronteira.append(i)
                        repetidos.append(i)
                        break

    def ordenar_fronteira(self, Fronteira):
        return sorted(Fronteira, key= valor)


    def a_estrela(self, Tabuleiro):
        inicio = time.time()
        fronteira = []
        repetidos  = []
        fronteira.append(Tabuleiro)
        nos_visitados = 0

        while len(fronteira) > 0:
            no_atual = fronteira.pop(0)
            repetidos.append(no_atual)
            nos_visitados = nos_visitados + 1
            #print('No atual: ')
            self.mostrar_tabuleiro(no_atual)

            if self.verificar_se_acabou(no_atual):
                fim = time.time()
                tempo = fim - inicio
                print('QTDD de nós testados: ')
                print(nos_visitados)
                print('Tempo de execução: ')
                print(tempo)
                print('Custo da solução: ')
                print(no_atual.nivel)
                print('Profundidade máxima: ')
                print(self.calcular_profundidade_maxima(repetidos))
                print('Custo do nós na fronteira / é tbm a quantidade de nós gerados: ')
                print(nos_visitados + len(fronteira))
                return no_atual
            else:
                filhos = self.filhos_nao_repitidos(repetidos, self.explorar_tabuleiro(no_atual))

                for i in filhos:
                    i.valor_da_funcao_F = (self.heuristica_pecas_fora_do_lugar(i) + i.nivel)
                    # f(x) = g(x) + h(x), sendo f(x) o i.valor_da_funcao_F, h(x) o 
                    # self.heuristica_pecas_fora_do_lugar(i) e g(x) o i.nivel
                    fronteira.append(i)  
                
                fronteira = self.ordenar_fronteira(fronteira)
            


sys.setrecursionlimit(2000)

#tab_inicial = [[1, 3, 0], [4, 2, 6], [7, 5, 8]]
#tab_inicial = [[5, 0, 2], [1, 4, 3], [7, 8, 6]]
#tab_inicial = [[0, 2, 3], [1, 5, 6], [4, 7, 8]]
#t = Tabuleiro(tab_inicial, 0, None)
#jogo = JogoDosOito()

#jogo.historico_de_jogadas(jogo.busca_cega_busca_profundidade(t))
#jogo.historico_de_jogadas(jogo.busca_cega_busca_largura(t))
#jogo.historico_de_jogadas(jogo.a_estrela(t))
#jogo.historico_de_jogadas(jogo.busca_heuristica(t))

#VLW