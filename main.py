import tkinter as tk
import subprocess

class Tabuleiro:
    validos = ['1', '2', '3', '4', '5', '6', '7', '8', '']
    contador = 0
    def __init__(self, parent):

        self.nValorAtual=0

        top = self.top = parent

        entry1 = tk.StringVar()
        self.btn1= tk.Entry(top, textvariable=entry1)
        entry1.set("1")
        
        entry2 = tk.StringVar()
        self.btn2= tk.Entry(top, textvariable=entry2)
        entry2.set("2")

        entry3 = tk.StringVar()
        self.btn3= tk.Entry(top, textvariable=entry3)
        entry3.set("3")

        entry4 = tk.StringVar()
        self.btn4= tk.Entry(top, textvariable=entry4)
        entry4.set("4")

        entry5 = tk.StringVar()
        self.btn5= tk.Entry(top, textvariable=entry5)
        entry5.set("5")

        entry6 = tk.StringVar()
        self.btn6= tk.Entry(top, textvariable=entry6)
        entry6.set("6")

        entry7 = tk.StringVar()
        self.btn7= tk.Entry(top, textvariable=entry7)
        entry7.set("7")

        entry8 = tk.StringVar()
        self.btn8= tk.Entry(top, textvariable=entry8)
        entry8.set("8")

        entry9 = tk.StringVar()
        self.btn9= tk.Entry(top, textvariable=entry9)
        entry9.set("")

        self.btn_largura = tk.Button(top, text="Largura", command=self.largura)
        self.btn_profundidade = tk.Button(top, text="Profundidade", command=self.profundidade)
        self.btn_guloso = tk.Button(top, text="Guloso", command=self.guloso)
        self.btn_a_estrela = tk.Button(top, text="A*", command=self.a_estrela)

    


        self.btn1.grid(row=1,column=0, padx= 1, pady=10)
        self.btn2.grid(row=1,column=1, padx= 1, pady=10)
        self.btn3.grid(row=1,column=2, padx= 1, pady=10)
        self.btn4.grid(row=2,column=0, padx= 1, pady=10)
        self.btn5.grid(row=2,column=1, padx= 1, pady=10)
        self.btn6.grid(row=2,column=2, padx= 1, pady=10)
        self.btn7.grid(row=3,column=0, padx= 1, pady=10)
        self.btn8.grid(row=3,column=1, padx= 1, pady=10)
        self.btn9.grid(row=3,column=2, padx= 1, pady=10)
        self.btn_largura.grid(row=4,column=0, padx= 1, pady=10)
        self.btn_profundidade.grid(row=4,column=1, padx= 1, pady=10)
        self.btn_guloso.grid(row=4,column=2, padx= 1, pady=10)
        self.btn_a_estrela.grid(row=5,column=1, padx= 1, pady=10)

    def validarMatriz(self, matriz):
        for item in self.validos:
            if item not in matriz[0] and item not in matriz[1] and item not in matriz[2]:
                print("Epa! Você digitou algo a mais.")
                return
        nova_matriz = ''
        for linha in matriz:
            nova_linha = ''
            for i in range(len(linha)):
                nova_linha += linha[i]
                if i != len(linha) - 1:
                    nova_linha += ','
            nova_matriz += nova_linha + '\n'
        return nova_matriz

    def largura(self):
        pos_0_0 = str(self.btn1.get())
        pos_0_1 = str(self.btn2.get())
        pos_0_2 = str(self.btn3.get())
        pos_1_0 = str(self.btn4.get())
        pos_1_1 = str(self.btn5.get())
        pos_1_2 = str(self.btn6.get())
        pos_2_0 = str(self.btn7.get())
        pos_2_1 = str(self.btn8.get())
        pos_2_2 = str(self.btn9.get())
        matriz = []
        matriz.append([])
        matriz.append([])
        matriz.append([])
        matriz[0].append(pos_0_0) 
        matriz[0].append(pos_0_1)
        matriz[0].append(pos_0_2)
        
        matriz[1].append(pos_1_0) 
        matriz[1].append(pos_1_1)
        matriz[1].append(pos_1_2)

        matriz[2].append(pos_2_0) 
        matriz[2].append(pos_2_1)
        matriz[2].append(pos_2_2)
        matriz = self.validarMatriz(matriz)
        print(matriz)
        if matriz != None:
            arquivo = open("tabuleiros\\tabuleiro" + str(self.contador) + '.txt', "w")
            arquivo.write(matriz)
            arquivo.close()
            args = "python jogadas.py " + "tabuleiro" + str(self.contador) + '.txt' + " 1"
            p = subprocess.Popen(args)
            self.contador += 1
    def profundidade(self):
        pos_0_0 = str(self.btn1.get())
        pos_0_1 = str(self.btn2.get())
        pos_0_2 = str(self.btn3.get())
        pos_1_0 = str(self.btn4.get())
        pos_1_1 = str(self.btn5.get())
        pos_1_2 = str(self.btn6.get())
        pos_2_0 = str(self.btn7.get())
        pos_2_1 = str(self.btn8.get())
        pos_2_2 = str(self.btn9.get())
        matriz = []
        matriz.append([])
        matriz.append([])
        matriz.append([])
        matriz[0].append(pos_0_0) 
        matriz[0].append(pos_0_1)
        matriz[0].append(pos_0_2)
        
        matriz[1].append(pos_1_0) 
        matriz[1].append(pos_1_1)
        matriz[1].append(pos_1_2)

        matriz[2].append(pos_2_0) 
        matriz[2].append(pos_2_1)
        matriz[2].append(pos_2_2)
        matriz = self.validarMatriz(matriz)
        print(matriz)
        if matriz != None:
            arquivo = open("tabuleiros\\tabuleiro" + str(self.contador) + '.txt', "w")
            arquivo.write(matriz)
            arquivo.close()
            args = "python jogadas.py " + "tabuleiro" + str(self.contador) + '.txt' + " 2"
            p = subprocess.Popen(args)
            self.contador += 1


    def guloso(self):
        pos_0_0 = str(self.btn1.get())
        pos_0_1 = str(self.btn2.get())
        pos_0_2 = str(self.btn3.get())
        pos_1_0 = str(self.btn4.get())
        pos_1_1 = str(self.btn5.get())
        pos_1_2 = str(self.btn6.get())
        pos_2_0 = str(self.btn7.get())
        pos_2_1 = str(self.btn8.get())
        pos_2_2 = str(self.btn9.get())
        matriz = []
        matriz.append([])
        matriz.append([])
        matriz.append([])
        matriz[0].append(pos_0_0) 
        matriz[0].append(pos_0_1)
        matriz[0].append(pos_0_2)
        
        matriz[1].append(pos_1_0) 
        matriz[1].append(pos_1_1)
        matriz[1].append(pos_1_2)

        matriz[2].append(pos_2_0) 
        matriz[2].append(pos_2_1)
        matriz[2].append(pos_2_2)
        matriz = self.validarMatriz(matriz)
        print(matriz)
        if matriz != None:
            arquivo = open("tabuleiros\\tabuleiro" + str(self.contador) + '.txt', "w")
            arquivo.write(matriz)
            arquivo.close()
            args = "python jogadas.py " + "tabuleiro" + str(self.contador) + '.txt' + " 3"
            p = subprocess.Popen(args)
            self.contador += 1

    def a_estrela(self):
        pos_0_0 = str(self.btn1.get())
        pos_0_1 = str(self.btn2.get())
        pos_0_2 = str(self.btn3.get())
        pos_1_0 = str(self.btn4.get())
        pos_1_1 = str(self.btn5.get())
        pos_1_2 = str(self.btn6.get())
        pos_2_0 = str(self.btn7.get())
        pos_2_1 = str(self.btn8.get())
        pos_2_2 = str(self.btn9.get())
        matriz = []
        matriz.append([])
        matriz.append([])
        matriz.append([])
        matriz[0].append(pos_0_0) 
        matriz[0].append(pos_0_1)
        matriz[0].append(pos_0_2)
        
        matriz[1].append(pos_1_0) 
        matriz[1].append(pos_1_1)
        matriz[1].append(pos_1_2)

        matriz[2].append(pos_2_0) 
        matriz[2].append(pos_2_1)
        matriz[2].append(pos_2_2)
        matriz = self.validarMatriz(matriz)
        print(matriz)
        if matriz != None:
            arquivo = open("tabuleiros\\tabuleiro" + str(self.contador) + '.txt', "w")
            arquivo.write(matriz)
            arquivo.close()
            args = "python jogadas.py " + "tabuleiro" + str(self.contador) + '.txt' + " 4"
            p = subprocess.Popen(args)
            self.contador += 1

def main():
    root = tk.Tk()

    tabuleiro = Tabuleiro(root)
    root.wait_window(tabuleiro.top)

    return 0
main()